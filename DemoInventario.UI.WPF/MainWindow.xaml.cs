﻿using DemoInventario.BIZ;
using DemoInventario.COMMON.Entidades;
using DemoInventario.COMMON.Interfaces;
using DemoInventario.COMMON;
using DemoInventario.DAL.NoSQL.Local;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DemoInventario.UI.WPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IProductoManager _productosManager;
        private Tools.Accion _accion;
        public MainWindow()
        {
            InitializeComponent();
            _productosManager = new ProductoManager(new ProductoRepository());
            AsignarContexto(null);
            _accion = Tools.Accion.Nada;
        }

        private void AsignarContexto(ProductoDTO p)
        {
            gridProducto.DataContext = p;
            if (p != null)
            {
                gridProducto.IsEnabled = true;
            }
            else
            {
                gridProducto.IsEnabled = false;
            }
            try
            {
                ActualizarGrid(_productosManager.ListarElementos);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ActualizarGrid(List<ProductoDTO> productos)
        {
            dtgProductos.ItemsSource = productos;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            AsignarContexto(new ProductoDTO());
            _accion = Tools.Accion.Nuevo;
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgProductos.SelectedItem != null)
            {
                AsignarContexto(dtgProductos.SelectedItem as ProductoDTO);
                _accion = Tools.Accion.Editar;
            }
            else
            {
                MessageBox.Show("Primero seleccione un producto", "Inventario", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (_accion)
            {
                case Tools.Accion.Editar:
                    try
                    {
                        _productosManager.Modificar(gridProducto.DataContext as ProductoDTO);
                        MessageBox.Show("Producto editado correctamente", "Inventario", MessageBoxButton.OK, MessageBoxImage.Information);
                        AsignarContexto(null);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    break;
                case Tools.Accion.Nuevo:
                    try
                    {
                        _productosManager.Agregar(gridProducto.DataContext as ProductoDTO);
                        MessageBox.Show("Producto guardado correctamente", "Inventario", MessageBoxButton.OK, MessageBoxImage.Information);
                        AsignarContexto(null);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    break;
                default:
                    break;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            AsignarContexto(null);
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (dtgProductos.SelectedItem != null)
            {
                if (MessageBox.Show("¿Realmente desea eliminar el producto?", "Confirmación", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        _productosManager.Eliminar(dtgProductos.SelectedItem as ProductoDTO);
                        MessageBox.Show("Producto Eliminado Correctamente", "Inventario", MessageBoxButton.OK, MessageBoxImage.Information);
                        AsignarContexto(null);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error:", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }


        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txbCriterio.Text))
            {
                switch (cmbFiltro.Text)
                {
                    case "Nombre":
                        ActualizarGrid(_productosManager.BuscarPorNombre(txbCriterio.Text));
                        break;
                    case "Unidad de Medida":
                        ActualizarGrid(_productosManager.BuscarProductosPorUnidadDeMedida(txbCriterio.Text));
                        break;
                    case "Descripción":
                        ActualizarGrid(_productosManager.BuscarProductosPorDescripcion(txbCriterio.Text));
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
