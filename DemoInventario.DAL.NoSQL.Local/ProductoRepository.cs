﻿using DemoInventario.COMMON.Entidades;
using DemoInventario.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoInventario.DAL.NoSQL.Local
{
    public class ProductoRepository:IGenericRepository<ProductoDTO>
    {
        private string _dbName = "Inventario.db";
        private string _tableName = "Productos";

        public List<ProductoDTO> Read
        {
            get
            {
                List<ProductoDTO> datos;
                using (var db = new LiteDatabase(_dbName))
                {
                    datos = db.GetCollection<ProductoDTO>(_tableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public ProductoDTO Create(ProductoDTO entidad)
        {
            using(var db=new LiteDatabase(_dbName))
            {
                var coleccion = db.GetCollection<ProductoDTO>(_tableName);
                coleccion.Insert(entidad);
            }
            return entidad;
        }

        public bool Delete(ProductoDTO entidad)
        {
            int r;
            using (var db = new LiteDatabase(_dbName))
            {
                var coleccion = db.GetCollection<ProductoDTO>(_tableName);
                r=coleccion.Delete(e => e.Id == entidad.Id);
            }
            return r > 0 ? true : false;
        }

        public ProductoDTO Update(ProductoDTO entidad)
        {
            using (var db = new LiteDatabase(_dbName))
            {
                var colection = db.GetCollection<ProductoDTO>(_tableName);
                colection.Update(entidad);
            }
            return entidad;
        }
    }
}
