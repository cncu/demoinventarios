﻿using DemoInventario.COMMON.Entidades;
using DemoInventario.COMMON.Interfaces;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoInventario.DAL.NoSQL.Azure
{
    public class ProductoRepository : IGenericRepository<ProductoDTO>
    {
        MobileServiceClient _cliente;
        IMobileServiceTable<ProductoDTO> _repo;
        public ProductoRepository()
        {
            _cliente = new MobileServiceClient("http://demoinventariocncu.azurewebsites.net");
            _repo = _cliente.GetTable<ProductoDTO>();
        }
        public List<ProductoDTO> Read
        {
            get
            {
                return _repo.IncludeTotalCount().ToListAsync().Result;
            }
        }

        public ProductoDTO Create(ProductoDTO entidad)
        {
            _repo.InsertAsync(entidad).RunSynchronously();
            return entidad;
        }

        public bool Delete(ProductoDTO entidad)
        {
            _repo.DeleteAsync(entidad).RunSynchronously();
            return true;
        }

        public ProductoDTO Update(ProductoDTO entidad)
        {
            _repo.UpdateAsync(entidad).RunSynchronously();
            return entidad;
        }
    }
}
