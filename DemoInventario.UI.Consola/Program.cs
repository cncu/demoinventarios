﻿using DemoInventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInventario.UI.Consola
{
    class Program
    {
        private static IProductoManager _productoManager;
        static void Main(string[] args)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("\tBienvenidos al sistema de inventarios");
                Console.WriteLine("\n1.- Gestionar Productos");
                Console.WriteLine("\n2.- Altas y bajas de inventario");
                Console.WriteLine("\n3.- Reportes");
                Console.WriteLine("\n\nQ.- Salir");
                Console.WriteLine("\n\n\tSeleccione una opción:");
                switch (Console.ReadLine())
                {
                    case "1":
                        GestionarProductos();
                        break;
                    case "2":
                        AltasBajas();
                        break;
                    case "3":
                        Reportes();
                        break;
                    case "Q":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("\nOpción invalida, presione una tecla para continuar...");
                        Console.ReadLine();
                        break;
                }
            } while (true);
        }

        private static void Reportes()
        {
            throw new NotImplementedException();
        }

        private static void AltasBajas()
        {
            throw new NotImplementedException();
        }

        private static void GestionarProductos()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("\tMenú de productos");
                Console.WriteLine("\n1.- Alta de productos");
                Console.WriteLine("\n2.- Mostrar todos los productos");
                Console.WriteLine("\n3.- Modificar un producto");
                Console.WriteLine("\n4.- Eliminar un producto");
                Console.WriteLine("\nR.- Regresar al menú anterior");
                Console.WriteLine("\n\n\tSeleccione una opción:");
                switch (Console.ReadLine())
                {

                    default:
                        break;
                }
            } while (true);
        }
    }
}
