﻿using DemoInventario.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInventario.COMMON
{
    public static class Factory
    {
        public static ProductoDTO ObtenerNuevoProducto(string nombre,string descripcion,string unidadDeMedida)
        {
            if (string.IsNullOrEmpty(nombre))
            {
                MandaExcepcion("Nombre");
            }
            if (string.IsNullOrEmpty(unidadDeMedida))
            {
                MandaExcepcion("Unidad de Medida");
            }
            return new ProductoDTO()
            {
                Cantidad = 0,
                Descripcion = descripcion,
                Id = Guid.NewGuid().ToString(),
                Nombre = nombre,
                UnidadDeMedida = unidadDeMedida
            };
        }

        private static void MandaExcepcion(string campo)
        {
            throw new Exception("El campo " + campo + " no puede estar vacío.");
        }
    }
}
