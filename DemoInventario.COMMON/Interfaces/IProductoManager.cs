﻿using DemoInventario.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInventario.COMMON.Interfaces
{
    public interface IProductoManager:IGenericManager<ProductoDTO>
    {
        List<ProductoDTO> BuscarPorNombre(string nombre);
        List<ProductoDTO> ProductosConStockMinimo();
        List<ProductoDTO> BuscarProductosPorUnidadDeMedida(string unidadDeMedida);
        List<ProductoDTO> BuscarProductosPorDescripcion(string descripcion);
        
    }
}
