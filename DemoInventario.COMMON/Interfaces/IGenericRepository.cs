﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInventario.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T: class
    {
        T Create(T entidad);
        List<T> Read { get;}
        T Update(T entidad);
        bool Delete(T entidad);
    }
}
