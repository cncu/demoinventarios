﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInventario.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:class
    {
        T Agregar(T entidad);
        List<T> ListarElementos { get; }
        T Modificar(T entidad);
        bool Eliminar(T entidad);
        T BuscarPorId(string id);
    }
}
