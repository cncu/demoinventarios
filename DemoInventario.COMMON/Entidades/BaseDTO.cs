﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoInventario.COMMON.Entidades
{
    /// <summary>
    /// Clase que permite eliminar los recursos de forma eficiente implementado la interfaz IDisposable; De esta clase deberán heredar todas las entidades del sistema que se almacenen. 
    /// </summary>
    public abstract class BaseDTO : IDisposable
    {
        public BaseDTO()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }
        private bool _isDisposed;
        public void Dispose()
        {
            if (!_isDisposed)
            {
                this._isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }
    }

}
