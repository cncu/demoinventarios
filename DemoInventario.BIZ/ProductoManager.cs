﻿using DemoInventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DemoInventario.COMMON.Entidades;

namespace DemoInventario.BIZ
{
    public class ProductoManager : IProductoManager
    {
        private IGenericRepository<ProductoDTO> _productoRepository;
        public ProductoManager(IGenericRepository<ProductoDTO> productoRepository)
        {
            _productoRepository = productoRepository;
        }
        public List<ProductoDTO> ListarElementos
        {
            get
            {
                return _productoRepository.Read;
            }
        }

        public ProductoDTO Agregar(ProductoDTO entidad)
        {
            return _productoRepository.Create(entidad);
        }


        public ProductoDTO BuscarPorId(string id)
        {
            //Select * from productos where id=id
            return ListarElementos.Where(p => p.Id == id).SingleOrDefault();
        }

        public List<ProductoDTO> BuscarPorNombre(string nombre)
        {
            return ListarElementos.Where(p => p.Nombre.ToLower().Contains(nombre.ToLower())).ToList();
        }

        public List<ProductoDTO> BuscarProductosPorDescripcion(string descripcion)
        {
            return ListarElementos.Where(p => p.Descripcion.ToLower().Contains(descripcion.ToLower())).ToList();
        }

        public List<ProductoDTO> BuscarProductosPorUnidadDeMedida(string unidadDeMedida)
        {
            return ListarElementos.Where(p => p.UnidadDeMedida.ToLower().Contains(unidadDeMedida.ToLower())).ToList();
        }


        public bool Eliminar(ProductoDTO entidad)
        {
            return _productoRepository.Delete(entidad);
        }

        public ProductoDTO Modificar(ProductoDTO entidad)
        {
            return _productoRepository.Update(entidad);
        }

        public List<ProductoDTO> ProductosConStockMinimo()
        {
            return ListarElementos.Where(p => p.Cantidad < 10).ToList();
        }
    }
}
